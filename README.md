### Codebase for gRPC microservice 

It's a shipping container management application running on multiple gRPC services. 
* There is User service for signing up users, authenticating them. Users can create consignments.
* consignment service let's users create new consignments and will deal with matching a consignment of containers to a vessel which is best suited to that consignment.
* Vessel service will find a vessel capable of handling that consignment. Both consignment and vessel service will talk to each other internally.
* A reactJS base UI to interact with the services.

